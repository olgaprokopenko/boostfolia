const gulp = require('gulp');
const sass = require('gulp-sass');
const image = require('gulp-image');
const concat = require('gulp-concat');
const pug = require('gulp-pug');
const browserSync = require('browser-sync');
const useref = require('gulp-useref');
const uglify = require('gulp-uglify');
const gulpif = require('gulp-if');
const autoprefixer = require('gulp-autoprefixer');
const del = require('del');
const cleanCSS = require('gulp-clean-css');
 

gulp.task('image', function () {
    gulp.src('./src/assets/img/*')
        .pipe(image())
        .pipe(gulp.dest('./build/assets/img'));
});

gulp.task('html', function () {
    gulp.src('./src/**/*.html')
        .pipe(useref())
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulp.dest('./build'));
});

gulp.task('pug', function () {
    gulp.src('./src/pages/**/*.pug')
        .pipe(pug())
        .pipe(concat('index.html'))
        .pipe(gulp.dest('./build'));
});

gulp.task('sass', function () {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(concat('style.css'))
        // .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./build/styles'));
});

gulp.task('browserSync', function () {
    browserSync({
        server: {
            baseDir: './build/'
        }
    });
});

gulp.task('fonts', function () {
    return gulp.src('./src/assets/fonts/**/*')
        .pipe(gulp.dest('./build/assets/fonts'))
});

gulp.task('clean', function () {
    return del.sync('build');
  });

gulp.task('watch', ['sass', 'pug', 'html', 'fonts', 'image', 'clean', 'browserSync'], function () {
    gulp.watch('./src/styles**/*.scss', ['sass']);
    gulp.watch('./src/assets/img/*', ['image']);
    gulp.watch('./src/**/*.html', ['html']);
    gulp.watch('./src/**/*.js', ['html']);
    gulp.watch('./src/pages/**/*.pug', ['pug']);
    gulp.watch('./src/assets/fonts/**/*', ['fonts']);
    gulp.watch('build/*.html', browserSync.reload);
    gulp.watch("./build/**/*.css").on("change", browserSync.reload);
    gulp.watch('./build/**/*.js').on("change", browserSync.reload);
});

gulp.task('default', ['watch', 'image']);